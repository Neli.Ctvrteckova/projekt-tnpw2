﻿using DataAccess.Model;

namespace App.Class
{
    public class TestArticle
    {
        public Test Test { get; set; }

        public Article Article { get; set; }
    }
}