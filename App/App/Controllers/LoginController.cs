﻿using App.Class;
using DataAccess.Dao;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace App.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(string login, string password)
        {
            if (Membership.ValidateUser(login, password.HashPassword()))
            {
                var userDao = new VyukaUserDao();
                var user = userDao.GetByLogin(login);
                user.LastActivityDay = DateTime.Today;
                userDao.Update(user);
                FormsAuthentication.SetAuthCookie(login, false);

                if (user.Role.Identificator.Equals(Role.Admin))
                {
                    return RedirectToAction("Index", "Home", new {area = "Admin"});
                }
                return RedirectToAction("IndexUser", "Home", new {area = "User"});
            }

            TempData["error"] = "Login nebo heslo není správné.";
            return RedirectToAction("Index", "Login");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("Index", "Home", new { Area = "" });
        }
    }
}