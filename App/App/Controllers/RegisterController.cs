﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using App.Class;
using App.Models;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Admin/Register
        public ActionResult Index()
        {
            VyukaUserDao vyukaUserDao = new VyukaUserDao();
            IList<VyukaUser> users = vyukaUserDao.GetAll();

            ViewBag.users = users;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(UserRegister userRegister )
        {
            if (ModelState.IsValid)
            {
                VyukaUserDao vyukaUserDao = new VyukaUserDao();


                if (vyukaUserDao.GetByLogin(userRegister.Login) == null)
                {
                    VyukaRole vyukaRole = new VyukaRoleDao().GetById(2);
                    VyukaUser user = new VyukaUser()
                    {
                        Login = userRegister.Login,
                        Name = userRegister.Name,
                        Surname = userRegister.Surname,
                        Password = userRegister.Password.HashPassword(),
                        Role = vyukaRole,
                        RegistrationDay = DateTime.Today,
                        LastActivityDay = DateTime.Today

                    };
                    
                    vyukaUserDao.Create(user);

                    FormsAuthentication.SetAuthCookie(user.Login, false);
                    return RedirectToAction("IndexUser", "Home", new {area = "User"});

                }

                ModelState.AddModelError("Login", "Uživatel již existuje. Zkuste to prosím znovu.");

                return View("Index", userRegister);
            }

            return View("Index", userRegister);

        }

    }
}
