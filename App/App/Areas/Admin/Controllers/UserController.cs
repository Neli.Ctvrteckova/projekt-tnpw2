﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.Admin.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class UserController : Controller
    {
        // GET: Admin/User
        public ActionResult UsersCategories(int id, int? page) //hlavní stránka s uživetli a stránkováním 
        {
            int itemsOnPage = 5;
            int pg = page.HasValue ? page.Value : 1;
            int totalUsers;

            IList<VyukaUser> users = new VyukaUserDao().GetUsersByCategoryPaged(id, itemsOnPage, pg, out totalUsers);
            ViewBag.Categories = new VyukaRoleDao().GetAll(); //zobtazení uživatelů podle jejich role (admin nebo uživatel)

            ViewBag.Pages = (int)Math.Ceiling((double)totalUsers / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexList", users);
            }
            return View("IndexList", users);
        }

        public ActionResult ChangeUserRole(int userId, int roleId)
        {
            VyukaUserDao userDao = new VyukaUserDao();
            VyukaUser user = userDao.GetById(userId); //načtení uživatele
            VyukaRoleDao roleDao = new VyukaRoleDao();
            user.Role = roleDao.GetById(roleId); //načtení jeho role 
            userDao.Update(user);
                
            int itemsOnPage = 5;
            int totalUsers;

            IList<VyukaUser> users = userDao.GetUsersByCategoryPaged(roleId, itemsOnPage, 1, out totalUsers); //stránkování podle rolí 
            ViewBag.Categories = roleDao.GetAll();

            ViewBag.Pages = (int)Math.Ceiling((double)totalUsers / (double)itemsOnPage);
            ViewBag.CurrentPage = 1;

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexList", users);
            }
            return View("IndexList", users);
        }

        public ActionResult DetailUser(int id)
        {
            VyukaUserDao vyukaUserDao = new VyukaUserDao();
            VyukaUser u = vyukaUserDao.GetById(id); //zobrazení detailů o uživateli 

            if (Request.IsAjaxRequest())
            {
                return PartialView(u);
            }
            return View(u);
        }

        public ActionResult SearchU(string phrase)
        {
            VyukaUserDao vyukaUserDao = new VyukaUserDao();
            ViewBag.Categories = new VyukaRoleDao().GetAll();
            IList<VyukaUser> users = vyukaUserDao.SearchUser(phrase);

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexList", users);
            }
            return View("IndexList", users);
        }

        public JsonResult SearchUser(string query)
        {
            VyukaUserDao vyukaUserDao = new VyukaUserDao();
            IList<VyukaUser> users = vyukaUserDao.SearchUser(query);

            List<string> names = (from VyukaUser a in users select a.Login).ToList();

            return Json(names, JsonRequestBehavior.AllowGet);
        }
    }
}