﻿using DataAccess.Dao;
using System.Web.Mvc;
using App.Class;

namespace App.Areas.Admin.Controllers
{
    [ChildActionOnly]
    [Authorize(Roles = Role.Admin)]
    public class MenuController : Controller
    {   
        public ActionResult Index() //ověření uživatelského oprávnění pro zobrazení správného menu 
        {
            var vyukaUserDao = new VyukaUserDao();
            var vyukaUser = vyukaUserDao.GetByLogin(User.Identity.Name);
            return View("AdminMenu", vyukaUser);
        }

        public ActionResult SidemenuList() //vytvoření pohledu pro boční menu
        {
            ViewBag.Categories = new VyukaRoleDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuText()
        {
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuVocabulary()
        {
            return PartialView();
        }

        public ActionResult SidemenuTest(int testId)
        {
            ViewBag.TestId = testId;
            return PartialView();
        }
    }
}