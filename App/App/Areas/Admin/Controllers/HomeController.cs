﻿using System.Web.Mvc;
using App.Class;

namespace App.Areas.Admin.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index() //vytvoření úvodní stránky
        {
            if (Request.IsAjaxRequest())
                {
                    return PartialView();
                }                
         return View();
        }
    }
}