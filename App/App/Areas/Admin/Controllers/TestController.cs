﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.WebPages;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.Admin.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class TestController : Controller
    {
        // GET: Admin/Test
        public ActionResult CreateTestPage(int testId, int articleId) //vytvoření nového prázdného testu, podle Id článku, ke kterému je přiřazen
        {
            TestDao testDao = new TestDao();
            var test = testDao.GetById(testId);
            ArticleDao articleDao = new ArticleDao();

            //pokud ještě test nebyl vytvořen
            if (test == null)
            {
                test = new Test
                {
                    //vytvoření prázdné otázky
                    Questions = new List<TestQuestions>()
                };
                test.Questions.Add(new TestQuestions
                {
                    Answers = new List<TestAnswer>(),
                    Question = "",
                    Hint = "",
                    Test = test
                });
                for (int i = 0; i < 4; i++)
                {
                    //ke každé otázce jsou vytvořeny 4 prázdné odpovědi, z nichž ani jedna není nastavená jako správná 
                    test.Questions[0].Answers.Add(new TestAnswer
                    {
                        Question = test.Questions[0],
                        Answer = "",
                        IsCorrect = false
                    });
                }
                testDao.SaveOrUpdate(test);
                Article article = articleDao.GetById(articleId);
                article.TestId = test.Id;
                articleDao.Update(article);
            }
          
            ViewBag.Nazev = articleDao.GetArticleByTestId(test.Id).Name;

            if (Request.IsAjaxRequest())
            {
                return PartialView(test);
            }
            return View(test);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTest(Test test) //metoda pro uložení testu
        {
            TestDao testDao = new TestDao();
            ViewBag.Nazev = new ArticleDao().GetArticleByTestId(test.Id).Name;
            bool isValid = true;
            foreach (var qe in test.Questions)
            {
                qe.Test = test;
                //aby byl test validní, otázka nesmí být prázdná 
                isValid = !qe.Question.IsEmpty();
                bool haveCorrectAnswer = false;
                foreach (var ans in qe.Answers)
                {
                    //validní odpověď nesmí být prázdná
                    isValid = !ans.Answer.IsEmpty();
                    ans.Question = qe;
                    //alespoň jedna z odpovědí musí být nastavena jako správná, jinak test není validní 
                    if (!haveCorrectAnswer)
                    {
                        haveCorrectAnswer = ans.IsCorrect;
                    }

                    if (!haveCorrectAnswer)
                    {
                        isValid = false;
                    }
                }
            }           

            //pokud je test validní, uloží se 
            if (isValid)
            {  
                testDao.SaveOrUpdate(test);
                if (test.Published)
                {
                    return RedirectToAction("IndexText", "Text");
                }

                if (Request.IsAjaxRequest())
                {
                    return PartialView("CreateTestPage", test);
                }
                return View("CreateTestPage", test);
            }

            //pokud test není validní, napíše o tom hlášení 
            TempData["error"] = "Všechna pole kromě nápovědy musí být vyplněna, zkontrolujte si, zda je v každé otázce vždy minimálně jedna odpověď správná.";
            if (Request.IsAjaxRequest())
            {
                return PartialView("CreateTestPage", test);
            }
            return View("CreateTestPage", test);
        }

        public ActionResult DeleteQuestion(int testId, int index)
        {
            TestDao testDao = new TestDao();
            ViewBag.Nazev = new ArticleDao().GetArticleByTestId(testId).Name;
            Test test = testDao.GetById(testId);

            //v každém testu musí být  alespoň jedna otázka 
            if (test.Questions.Count > 1)
            {
                test.Questions.RemoveAt(index);
                testDao.SaveOrUpdate(test);
            }
            else
            {
                TempData["error"] = "Test musí obsahovat alespoň jednu otázku.";
            }
            
            if (Request.IsAjaxRequest())
            {
                return PartialView("CreateTestPage",test);
            }
            return View("CreateTestPage", test);
        }

        public ActionResult AddQuestion(int testId)
        {
            TestDao testDao = new TestDao();
            Test test = testDao.GetById(testId);
            ViewBag.Nazev = new ArticleDao().GetArticleByTestId(testId).Name;
            //přidá se nová otázka, která opět obsahuje prázdné pole pro otázku a k tomu 4 odpovědi, z nichž ani jedna není správná
            test.Questions.Add(new TestQuestions
            {
                Answers = new List<TestAnswer>(),
                Test = test,
                Question = "",
                Hint = ""
            });
            for (int i = 0; i < 4; i++)
            {
                test.Questions[test.Questions.Count-1].Answers.Add(new TestAnswer
                {
                    Answer = "",
                    IsCorrect = false,
                    Question = test.Questions[test.Questions.Count - 1]
                });
            }
            testDao.SaveOrUpdate(test);
            if (Request.IsAjaxRequest())
            {
                return PartialView("CreateTestPage", test);
            }
            return View("CreateTestPage", test);
        }
    }
}