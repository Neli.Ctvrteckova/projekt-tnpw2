﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.Admin.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class VocabularyController : Controller
    {
        // GET: Admin/Vocabulary
        public ActionResult IndexVocabulary(int? page) //hlavní stránka se slovíčky a stránkováním
        {
            int itemsOnPage = 5; //na jedné stránce bude 5 položek
            int pg = page.HasValue ? page.Value : 1; 
            int totalWords;

            VocabularyDao vocabularyDao = new VocabularyDao();
            IList<Vocabulary> vocabulary = vocabularyDao.GetVocabularyPaged(itemsOnPage, pg, out totalWords);

            ViewBag.Pages = (int)Math.Ceiling((double)totalWords/ (double)itemsOnPage); //spočítání celkového počtu záložek podle položek seznamu
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView(vocabulary);
            }
            return View(vocabulary);       
        }

        public JsonResult SearchWords(string query)
        {
            VocabularyDao vocabularyDao = new VocabularyDao();
            IList<Vocabulary> vocabulary = vocabularyDao.SearchWord(query);

            List<string> names = (from Vocabulary a in vocabulary select a.Word).ToList();

            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchV(string phrase)
        {
            VocabularyDao vocabularyDao = new VocabularyDao();
            ViewBag.Words = vocabularyDao.GetAll();
            IList<Vocabulary> vocabulary = vocabularyDao.SearchWord(phrase);

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexVocabulary", vocabulary);
            }
            return View("IndexVocabulary", vocabulary);
        }

        public ActionResult EditVocabulary(int id)
        {
            VocabularyDao vocabularyDao = new VocabularyDao();
            Vocabulary vocabulary = vocabularyDao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(vocabulary);
            }
            return View(vocabulary);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateVocabulary(Vocabulary vocabulary)
        {
            if (ModelState.IsValid)
            {
                VocabularyDao vocabularyDao = new VocabularyDao();
                vocabularyDao.Update(vocabulary);
                TempData["message-success"] = "Slovíčko " + vocabulary.Word + " bylo upraveno.";
                return RedirectToAction("IndexVocabulary", "Vocabulary");
            }
            return View("EditVocabulary", vocabulary);
        }

        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateVocabulary(Vocabulary vocabulary)
        {
            if (ModelState.IsValid)
            {
                VocabularyDao vocabularyDao = new VocabularyDao();

                if (vocabularyDao.GetByWord(vocabulary.Word) == null) //ověření, zda slovíčko již neexistuje
                {
                    vocabularyDao.Create(vocabulary);
                    TempData["message-success"] = "Slovíčko " + vocabulary.Word + " bylo vytvořeno.";
                    return RedirectToAction("IndexVocabulary", "Vocabulary");
                }
                ModelState.AddModelError("Word", "Toto slovíčko již existuje.");
            }
            return View("Create", vocabulary);
        }

        public ActionResult Delete(int id)
        {
            VocabularyDao vocabularyDao = new VocabularyDao();
            Vocabulary vocabulary = vocabularyDao.GetById(id);
            vocabularyDao.Delete(vocabulary);
            TempData["message-success"] = "Slovíčko " + vocabulary.Word + " bylo smazáno.";
            return RedirectToAction("IndexVocabulary", "Vocabulary");
        }
    }
}