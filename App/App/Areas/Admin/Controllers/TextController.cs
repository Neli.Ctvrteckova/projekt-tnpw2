﻿using DataAccess.Dao;
using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Class;

namespace App.Areas.Admin.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class TextController : Controller
    {
        // GET: Admin/Text
        public ActionResult IndexText() //vytvoření úvodní stránky v zálžoce správa textů
        {
            ArticleDao articleDao = new ArticleDao();
            IList<Article> articles = articleDao.GetAll(); //načtení všech článků
            IList<TestArticle> ta = new List<TestArticle>(); //načtení testů pro články

            foreach (Article a in articles)
            {
                //pokud článek nemá vytvořený test, nezobrazí se 
                if (a.TestId == 0)
                {
                    ta.Add(new TestArticle() { Article = a, Test = null });
                }
                else
                {
                    //pokud článek má svůj test, zobrazí se 
                    Test t = new TestDao().GetById(a.TestId);
                    ta.Add(new TestArticle() { Article = a, Test = t });
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView(ta);
            }
            return View(ta);
        }

        public ActionResult Detail(int id) 
        {
            ArticleDao articleDao = new ArticleDao();
            Article a = articleDao.GetById(id); //načtení konkrétního článku podle jeho Id

            if (Request.IsAjaxRequest())
            {
                return PartialView(a);
            }
            return View(a);
        }

        public ActionResult Category(int id)
        {
            IList<Article> article = new ArticleDao().GetArticlesInCategoryId(id); //zobrazení článků v určité kategorii
            ViewBag.Categories = new ArticleCategoryDao().GetAll(); //zobrazení všech kategorií 
            IList<TestArticle> ta = new List<TestArticle>();

            foreach (Article a in article)
            {
                if (a.TestId == 0)
                {
                    ta.Add(new TestArticle() { Article = a, Test = null });
                }
                else
                {
                    Test t = new TestDao().GetById(a.TestId);
                    ta.Add(new TestArticle() { Article = a, Test = t });
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexText", ta);

            }
            return View("IndexText", ta);
        }

        public JsonResult SearchArticles(string query)
        {
            ArticleDao articleDao = new ArticleDao();
            IList<Article> articles = articleDao.SearchArticle(query); //vyhledání článku podle jeho jména

            List<string> names = (from Article a in articles select a.Name).ToList();

            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchT(string phrase)
        {
            ArticleDao articleDao = new ArticleDao();
            ViewBag.Categories = new ArticleCategoryDao().GetAll(); //načtení všech článků
            IList<Article> article = articleDao.SearchArticle(phrase); //načtení odpovídajících článků podle kritéria 
            IList<TestArticle> ta = new List<TestArticle>();

            foreach (Article a in article)
            {
                if (a.TestId == 0)
                {
                    ta.Add(new TestArticle() { Article = a, Test = null });
                }
                else
                {
                    Test t = new TestDao().GetById(a.TestId);
                    ta.Add(new TestArticle() { Article = a, Test = t });
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexText", ta);
            }
            return View("IndexText", ta);
        }

        public ActionResult EditCategory(int id)
        {
            ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
            ArticleCategory categories = articleCategoryDao.GetById(id); //zobrazení konkrétní kategorie podle jejího Id
            if (Request.IsAjaxRequest())
            {
                return PartialView(categories);
            }
            return View(categories);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateCategory(ArticleCategory category)
        {
            if (ModelState.IsValid)
            {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                articleCategoryDao.Update(category); //upravení informací o kategorii 
                TempData["message-success"] = "Kategorie " + category.Name + " byla upravena.";
                return RedirectToAction("IndexText", "Text");
            }
            return View("EditCategory", category);
        }

        public ActionResult Create() //vytvoření nové kategorie 
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(ArticleCategory category)
        {
            if (ModelState.IsValid) { 
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();

                if (articleCategoryDao.GetByName(category.Name) == null) //ověření, zda nová kategorie nenese název jako už vytvořená kategorie
                {
                    articleCategoryDao.Create(category);
                    TempData["message-success"] = "Kategorie " + category.Name + " byla vytvořena.";
                    return RedirectToAction("IndexText", "Text");
                }   
                ModelState.AddModelError("Name", "Tato kategorie již existuje.");
            }
            return View("Create", category);
        }

        public ActionResult Delete(int id) //odstranění článku
        {
            ArticleDao articleDao = new ArticleDao();
            Article article = articleDao.GetById(id); //načtení konkrétního článku podle jeho Id

            TestDao testDao = new TestDao();
            Test test = testDao.GetById(article.TestId); //načtení testu podle toho, ke kterému článku patří 

            TestFinishedDao testFinishedDao = new TestFinishedDao();
            IList<TestFinished> testFinishedList = testFinishedDao.GetTestFinshedByTestId(test.Id); //načtení vyhodnocených testů pro jednotlivé články
            foreach (TestFinished testFinished in testFinishedList) //projít a odstranit všechny výskyty vyhodnoceného testu pro mazaný test
            {
                testFinishedDao.Delete(testFinished);
            }
            articleDao.Delete(article); //smazání článku
            testDao.Delete(test); //smazání testu, který byl ke článku přiřazen
            TempData["message-success"] = "Článek " + article.Name + " byl smazán.";
            return RedirectToAction("IndexText", "Text");
        }

        public ActionResult DeleteCategory(int id)
        {
            try {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                ArticleCategory articleCategory = articleCategoryDao.GetById(id);
                articleCategoryDao.Delete(articleCategory);
                TempData["message-success"] = "Kategorie " + articleCategory.Name + " byla smazána.";
            }
            catch (Exception)
            {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                ArticleCategory articleCategory = articleCategoryDao.GetById(id);
                TempData["error"] = "Kategorie " + articleCategory.Name + " nemůže být smazána, protože obsahuje články.";
            }
            return RedirectToAction("IndexText", "Text");
            }

        public ActionResult CreateArticle()
        {
            ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
            IList<ArticleCategory> categories = articleCategoryDao.GetAll(); //načtení všech kategorií, kam může být článek zařezen

            ViewBag.Categories = categories;

            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ArticleCreation(Article article, int categoryId, string name)
        {
            if (ModelState.IsValid)
            {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                ArticleCategory articleCategory = articleCategoryDao.GetById(categoryId); //přiřazení konkrétní kategorie ke článku
                article.Category = articleCategory;
                ArticleDao articleDao = new ArticleDao();
                Article a = articleDao.GetByName(name); // ověření názvu článku
                if (a == null)
                {
                    articleDao.Create(article);
                    TempData["message-success"] = "Článek byl úspěšně vytvořen.";
                }
                else
                {
                    IList<ArticleCategory> categories = articleCategoryDao.GetAll();
                    ViewBag.Categories = categories;
                    TempData["error"] = "Článek se stejným názvem již existuje.";
                    if (Request.IsAjaxRequest())
                    {
                        return PartialView("CreateArticle", article);
                    }
                    return View("CreateArticle", article);
                }            
            }

            else
            {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                IList<ArticleCategory> categories = articleCategoryDao.GetAll();
                ViewBag.Categories = categories;
                if (Request.IsAjaxRequest())
                {
                    return PartialView("CreateArticle", article);
                }
                return View("CreateArticle", article);
            }
            return RedirectToAction("IndexText");
        }

        [HttpPost]
        public string PictureArticle() //uložení obrázku článku do složky 
        {
            string fileName = "";
            HttpPostedFileBase file = Request.Files["file"];
            if (file != null) { 
                fileName = Path.GetFileName(file.FileName);
                string path = Server.MapPath("~/uploads/Article/" + fileName);
                file.SaveAs(path);
            }
            return "/uploads/Article/" + fileName;
        }

        public ActionResult EditArticle(int id)
        {
            ArticleDao articleDao = new ArticleDao();
            ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();

            Article a = articleDao.GetById(id);
            var cat = new List<SelectListItem>();
            foreach (var temp in articleCategoryDao.GetAll()) //vytvoření dropdownboxu s předvybranou kategorií
            {
               cat.Add(new SelectListItem()
                   {
                       Text = temp.Name,
                       Value = temp.Id.ToString(),
                       Selected = temp.Id == a.Category.Id? true : false
                   }
               ); 
            }

            ViewBag.Categories = cat;

            if (Request.IsAjaxRequest())
            {
                return PartialView(a);
            }
            return View(a);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ArticleEditation(Article article, int categoryId)
        {
            if (ModelState.IsValid)
            {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                ArticleCategory articleCategory = articleCategoryDao.GetById(categoryId);

                article.Category = articleCategory;

                ArticleDao articleDao = new ArticleDao();
                articleDao.Update(article);
                TempData["message-success"] = "Článek " + article.Name +" byl úspěšně upraven.";
                return RedirectToAction("IndexText");
            }
            else
            {
                ArticleCategoryDao articleCategoryDao = new ArticleCategoryDao();
                IList<ArticleCategory> categories = articleCategoryDao.GetAll();

                ViewBag.Categories = categories;
                if (Request.IsAjaxRequest())
                {
                    return PartialView("EditArticle", article);
                }
                return View("EditArticle", article);
            }
        }

        public void DeletePicture(string fileName) //odstranění obrázku článku ze složky, do které se předtím uložil
        {
            if(System.IO.File.Exists(Server.MapPath("~/uploads/Article/" + fileName))) { 

                System.IO.File.Delete(Server.MapPath("~/uploads/Article/" + fileName));
            }
        }
    }
}