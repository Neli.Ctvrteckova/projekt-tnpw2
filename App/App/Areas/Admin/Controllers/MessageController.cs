﻿using System.Collections.Generic;
using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.Admin.Controllers
{
    [Authorize(Roles = Role.Admin)]
    public class MessageController : Controller
    {
        // GET: Admin/Message
        public ActionResult Index() //zobrazení všech přijatých zpráv 
        {
            IList<Message> messages = new MessageDao().GetAll();
            if (Request.IsAjaxRequest())
            {
                return PartialView(messages);
            }
            return View(messages);
        }

        public ActionResult GetMessageDetail(int id) //zobrazení detailu zprávy podle jejího ID
        {
            MessageDao messageDao = new MessageDao();
            Message message = messageDao.GetById(id);

            if (Request.IsAjaxRequest())
            {
                return PartialView(message);
            }

            return View(message);
        }

        public ActionResult DeleteMessage(int id) //odstranění zprávy podle jejího ID
        {
            MessageDao messageDao = new MessageDao();
            Message message = messageDao.GetById(id);
            messageDao.Delete(message);
            IList<Message> messages = new MessageDao().GetAll();
            if (Request.IsAjaxRequest())
            {
                return PartialView("Index", messages);
            }
            return View("Index", messages);
        }
    }
}