﻿using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.User.Controllers
{
    [Authorize(Roles = Role.User)]
    public class MessageController : Controller
    {
        // GET: User/Text
        public ActionResult Index()
        {
            Message message = new Message()
            {
                //nalezení jména (přezdívky) uživatele, který píše zprávu
                UserName = User.Identity.Name
            };
           
            if (Request.IsAjaxRequest())
            {
                return PartialView(message);
            }
            return View(message);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendMessage(Message message)
        {
            if (ModelState.IsValid)
            {
                new MessageDao().Create(message);
                TempData["message-success"] = "Vaše zpárva byla úspěšně odeslána.";
                message = new Message()
                {
                    UserName = User.Identity.Name,
                    Sender = "",
                    Subject = "",
                    Text = ""
                };
                return RedirectToAction("Index", message);
            }
        
            if (Request.IsAjaxRequest())
                {
                    return PartialView("Index",message);
                }           
            return View("Index",message);
        }
    }
}