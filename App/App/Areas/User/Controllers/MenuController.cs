﻿using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;

namespace App.Areas.User.Controllers
{
    [ChildActionOnly]
    [Authorize(Roles = Role.User)]
    public class MenuController : Controller
    {
        // GET: User/Menu
        public ActionResult Index()
        {
            var vyukaUserDao = new VyukaUserDao();
            var vyukaUser = vyukaUserDao.GetByLogin(User.Identity.Name);
            return View("UserMenu", vyukaUser);
        }

        public ActionResult SidemenuProfil()
        {
            return PartialView();
        }

        public ActionResult SidemenuVocabulary()
        {
            return PartialView();
        }

        public ActionResult SidemenuText()
        {
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuTest()
        {
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuTestStart()
        {
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuTestSubmit()
        {
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuHome()
        {
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            return PartialView();
        }

        public ActionResult SidemenuMessage()
        {
            return PartialView();
        }
    }
}