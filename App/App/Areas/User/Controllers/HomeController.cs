﻿using System.Web.Mvc;
using App.Class;

namespace App.Areas.User.Controllers
{
    [Authorize(Roles = Role.User)]
    public class HomeController : Controller
    {
        // GET: User/Home
        public ActionResult IndexUser()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult KnowProfile()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult KnowText()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult KnowVocabulary()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult KnowMessage()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }
    }
}