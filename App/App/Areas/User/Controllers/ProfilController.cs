﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using App.Areas.User.Class;
using App.Class;
using App.Models;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.User.Controllers
{
    [Authorize(Roles = Role.User)]
    public class ProfilController : Controller
    {
        // GET: User/Profil
        public ActionResult EditProfil()
        {
            VyukaUserDao vyukaUserDao = new VyukaUserDao();
            VyukaUser user = vyukaUserDao.GetByLogin(User.Identity.Name);

            if (Request.IsAjaxRequest())
            {
                return PartialView(user);
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(VyukaUser user, HttpPostedFileBase picture)
        {
            if (ModelState.IsValid)
            {
                VyukaUserDao vyukaUserDao = new VyukaUserDao();

                if (picture != null)
                {
                    //nahrávané obrázky mohou být .jpg nebop .png
                    if (picture.ContentType == "image/jpeg" || picture.ContentType == "image/png")
                    {
                        Image image = Image.FromStream(picture.InputStream);

                        //jedinečný identifikátor
                        Guid guid = Guid.NewGuid();
                        string imageName = guid + ".jpg";

                        //pokud je velikost obrázku větší než 1000x1000px, zmenší se jeho velikos na maximální 1000x1000px
                        if (image.Height > 1000 || image.Width > 1000)
                        {
                            Image smallImage = ImageHelper.ScaleImage(image, 1000, 1000);
                            //vytvoření v rastrové grafice 
                            Bitmap b = new Bitmap(smallImage);

                            //uložení obrázku s jeho jedinečným identifikátorem do dbs ve formátu .jpg
                            b.Save(Server.MapPath("~/uploads/profilePictures/" + imageName), ImageFormat.Jpeg);

                            //uvolnení prostředků
                            smallImage.Dispose();
                            b.Dispose();
                        }
                        else
                        {
                            picture.SaveAs(Server.MapPath("~/uploads/profilePictures/" + imageName));
                        }

                        //přemazání aktuálního profilového obrázku 
                        if (!user.Image.IsEmpty())
                        {
                            System.IO.File.Delete(Server.MapPath("~/uploads/profilePictures/" + user.Image));
                        }
                        user.Image = imageName;
                    }
                }
                vyukaUserDao.Update(user);
                TempData["message-success"] = "Údaje uživatele " + user.Login + " byly upraveny";
            }
            return RedirectToAction("Profil");
        }

        public ActionResult Profil()
        {
            var vyukaUserDao = new VyukaUserDao();
            var user = vyukaUserDao.GetByLogin(User.Identity.Name);

            if (Request.IsAjaxRequest())
            {
                return PartialView(user);
            }

            return View(user);
        }

        public ActionResult Password()
        {

            ViewBag.Login = User.Identity.Name;
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(UserChangePassword user)
        {
            //ověření loginu a jeho hesla s jeho heshovanou podobou
            if (new VyukaUserDao().GetByLoginAndPassword(user.Login, user.CurrPass.HashPassword()) != null)
            {
                if (ModelState.IsValid)
                {
                    var vyukaUserDao = new VyukaUserDao();
                    var vyukaUser = vyukaUserDao.GetByLogin(user.Login);

                    vyukaUser.Password = user.NewPass.HashPassword();
                    vyukaUserDao.Update(vyukaUser);

                    TempData["message-success"] = "Heslo uživatele " + user.Login + " bylo upraveno";
                }

                if (Request.IsAjaxRequest()) return PartialView("Password");
                return View("Password");
            }

            ModelState.AddModelError("CurrPass",
                "Heslo uživatele " + user.Login + " bylo zadáno nesprávně, zkusteto znovu.");

            if (Request.IsAjaxRequest()) return PartialView("Password");

            return View("Password");
        }

        public ActionResult FinishedTestList(int? page)
        {
            int itemsOnPage = 10;
            int pg = page.HasValue ? page.Value : 1;

            TestFinishedDao testFinishedDao = new TestFinishedDao();
            //načtení ukončených testů konkrétního uživatele a jejich případné rozstránkování 
            IList<TestFinished> tests = testFinishedDao.GetTestListByUserAndPage(new VyukaUserDao().GetByLogin(User.Identity.Name).Id, itemsOnPage, pg, out var totalWords);

            ViewBag.Pages = (int)Math.Ceiling((double)totalWords / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            IList<TestName> testNames = new List<TestName>();
            ArticleDao articleDao = new ArticleDao();
            //přiřazení jména článku ke každému ukončenému testu
            foreach (TestFinished test in tests)
            {
                testNames.Add(new TestName
                {
                    TestFinished = test,
                    Article = articleDao.GetArticleByTestId(test.TestId)
                });
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView(testNames);
            }
            return View(testNames);
        }
    }
}