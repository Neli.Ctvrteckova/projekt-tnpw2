﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Areas.User.Class;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.User.Controllers
{
    [Authorize(Roles = Role.User)]
    public class TextController : Controller
    {
        // GET: User/Text
        public ActionResult IndexTextUser()
        {
            ArticleDao articleDao = new ArticleDao();
            IList<Article> articles = articleDao.GetAll();
            IList<TestArticle> ta = new List<TestArticle>();

            foreach (Article a in articles)
            {
                if (a.TestId == 0)
                {
                    ta.Add(new TestArticle(){Article = a, Test = null});
                }
                else
                {
                    Test t = new TestDao().GetById(a.TestId);
                    ta.Add(new TestArticle(){Article = a, Test = t});
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView(ta);
            }
            return View(ta);
        }

        public ActionResult Detail(int id)
        {
            ArticleDao articleDao = new ArticleDao();
            Article a = articleDao.GetById(id);

            ViewBag.Categories = new ArticleCategoryDao().GetAll();

            if (Request.IsAjaxRequest())
            {
                return PartialView(a);
            }
            return View(a);
        }

        public ActionResult CategoryUser(int id)
        {
            IList<Article> article = new ArticleDao().GetArticlesInCategoryId(id);
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            IList<TestArticle> ta = new List<TestArticle>();

            foreach (Article a in article)
            {
                if (a.TestId == 0)
                {
                    ta.Add(new TestArticle() { Article = a, Test = null });
                }
                else
                {
                    Test t = new TestDao().GetById(a.TestId);
                    ta.Add(new TestArticle() { Article = a, Test = t });
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexTextUser", ta);
            }
            return View("IndexTextUser", ta);
        }

        public JsonResult SearchArticles(string query)
        {
            ArticleDao articleDao = new ArticleDao();
            IList<Article> articles = articleDao.SearchArticle(query);

            List<string> names = (from Article a in articles select a.Name).ToList();

            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchTu(string phrase)
        {
            ArticleDao articleDao = new ArticleDao();
            ViewBag.Categories = new ArticleCategoryDao().GetAll();
            IList<Article> article = articleDao.SearchArticle(phrase);
            IList<TestArticle> ta = new List<TestArticle>();

            foreach (Article a in article)
            {
                if (a.TestId == 0)
                {
                    ta.Add(new TestArticle() { Article = a, Test = null });
                }
                else
                {
                    Test t = new TestDao().GetById(a.TestId);
                    ta.Add(new TestArticle() { Article = a, Test = t });
                }
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexTextUser", ta);
            }
            return View("IndexTextUser", ta);
        }
    }
}