﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.User.Controllers
{
    [Authorize(Roles = Role.User)]
    public class VocabularyController : Controller
    {
        // GET: User/Vocabulary
        public ActionResult IndexVocabularyUser(int? page)
        {
            int itemsOnPage = 5;
            int pg = page.HasValue ? page.Value : 1; 
            int totalWords;

            VocabularyDao vocabularyDao = new VocabularyDao();
            IList<Vocabulary> vocabulary = vocabularyDao.GetVocabularyPaged(itemsOnPage, pg, out totalWords);

            ViewBag.Pages = (int)Math.Ceiling((double)totalWords / (double)itemsOnPage);
            ViewBag.CurrentPage = pg;

            if (Request.IsAjaxRequest())
            {
                return PartialView(vocabulary);
            }
            return View(vocabulary);
        }

        public JsonResult SearchWords(string query)
        {
            VocabularyDao vocabularyDao = new VocabularyDao();
            IList<Vocabulary> vocabulary = vocabularyDao.SearchWord(query);

            List<string> names = (from Vocabulary a in vocabulary select a.Word).ToList();

            return Json(names, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchVu(string phrase)
        {
            VocabularyDao vocabularyDao = new VocabularyDao();
            ViewBag.Words = vocabularyDao.GetAll();
            IList<Vocabulary> vocabulary = vocabularyDao.SearchWord(phrase);

            if (Request.IsAjaxRequest())
            {
                return PartialView("IndexVocabularyUser", vocabulary);
            }
            return View("IndexVocabularyUser", vocabulary);
        }
    }
}