﻿using System;
using System.Web.Mvc;
using App.Class;
using DataAccess.Dao;
using DataAccess.Model;

namespace App.Areas.User.Controllers
{
    [Authorize(Roles = Role.User)]

public class TestController : Controller
    {
        // GET: User/Test
        public ActionResult Index(int id)
        {
            //načtení testu podle jeho Id
            Test test = new TestDao().GetById(id);
            //zobrazení dokončených testů konkrétního uživatele
            ViewBag.TestList =
                new TestFinishedDao().GetTestListByUserIdAndTestId(new VyukaUserDao().GetByLogin(User.Identity.Name).Id,
                    id);
            if (Request.IsAjaxRequest())
            {
                return PartialView(test);
            }
            return View(test);
        }

        public ActionResult StartTest(int id)
        {
            Test test = new TestDao().GetById(id);

            ViewBag.Nazev = new ArticleDao().GetArticleByTestId(id).Name;
            foreach (TestQuestions question in test.Questions)
            {
                foreach (TestAnswer answer in question.Answers)
                {
                    answer.IsCorrect = false;
                }
            }
            if (Request.IsAjaxRequest())
            {
                return PartialView(test);
            }
            return View(test);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitTest(Test testFilled)
        {
            //přiřazení id vyplněného testu 
            Test test = new TestDao().GetById(testFilled.Id);

            byte score = 0;
            byte maxScore = 0;

            for (int i = 0; i < test.Questions.Count; i++)
            {
                for (int j = 0; j < test.Questions[i].Answers.Count; j++)
                {
                    //ověření zda byla vyplněná odpověď správná
                    bool filledAnswer = testFilled.Questions[i].Answers[j].IsCorrect;
                    //ověření zda je testová otázka označená jako správná 
                    bool testAnswer = test.Questions[i].Answers[j].IsCorrect;
                    //ověření zda byla vybrána správná nebo špatná odpověď odpověď 
                    testFilled.Questions[i].Answers[j].WasCorrect = filledAnswer == testAnswer;

                    if (testAnswer && filledAnswer)
                    {
                        score++;
                    }
                    if(testAnswer)
                    {
                        maxScore++;
                    }
                }
            }

            ViewBag.Score = score;
            ViewBag.MaxScore = maxScore;
            ViewBag.Nazev = new ArticleDao().GetArticleByTestId(test.Id).Name;

            //vytvoření ukončeného testu podle uživatele
            new TestFinishedDao().Create(new TestFinished()
            {
                CompletingTest = DateTime.Now,
                MaxScore = maxScore,
                Score = score,
                TestId = test.Id,
                UserId = new VyukaUserDao().GetByLogin(User.Identity.Name).Id
            });

            if (Request.IsAjaxRequest())
            {
                return PartialView(testFilled);
            }
            return View(testFilled);
        }
    }
}