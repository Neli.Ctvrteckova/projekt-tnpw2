﻿using System.Web.Mvc;

namespace App.Areas.User
{
    public class UserAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "User";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "User_default",
                "User/{controller}/{action}/{id}",
                new { controller = "Home", action = "IndexUser", id = UrlParameter.Optional },
                namespaces: new[] { "App.Areas.User.Controllers" }
            );
        }
    }
}