﻿using DataAccess.Model;

namespace App.Areas.User.Class
{
    public class TestName
    {
        //pomocná třída pro přiřazení ukončenému testu název článku
        public TestFinished TestFinished { get; set; }

        public Article Article { get; set; }
    }
}