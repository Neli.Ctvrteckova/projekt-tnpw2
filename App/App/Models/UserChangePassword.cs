﻿using System.ComponentModel.DataAnnotations;

namespace App.Models
{
    public class UserChangePassword
    {
        [Required]
        public string Login { get; set; }

        [Required(ErrorMessage = "Staré heslo nebylo zadáno správně.")]
        [DataType(DataType.Password)]
        [Display(Name = "Staré heslo")]
        public string CurrPass { get; set; }

        [Required(ErrorMessage = "Nové heslo nesplňuje požadavky, musí obsahovat minimálně pět znaků.")]
        [MinLength(5)]//minimální délka hesla   
        [MaxLength(30)]//maximální délka hesla
        [DataType(DataType.Password)]
        public string NewPass { get; set; }

        [Required(ErrorMessage = "Toto pole je povinné..")]
        [DataType(DataType.Password)]
        [Compare("NewPass", ErrorMessage = "Hesla se neshodují.")] //porovnání hesel
        public string NewPassRepeat { get; set; }
    }
}