﻿using System.ComponentModel.DataAnnotations;

namespace App.Models
{
    public class UserRegister
    {
        [Required(ErrorMessage = "Vaše jméno je vyžadováno.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vaše příjmení je vyžadováno.")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Vaši přezdívku musíte vyplnit.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Heslo musíte vyplnit.")]
        [MinLength(5, ErrorMessage = "Heslo musí obsahovat minimálně pět znaků.")]
        [MaxLength(30)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Toto pole je povinné..")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Hesla se neshodují.")]
        public string PasswordRepeat { get; set; }
    }
}