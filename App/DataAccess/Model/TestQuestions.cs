﻿using System.Collections.Generic;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class TestQuestions : IEntity
    {
        public virtual int Id { get; set; }

        public virtual Test Test { get; set; }

        public virtual string Question { get; set; }

        public virtual string Hint { get; set; }

       public virtual IList<TestAnswer> Answers { get; set; }
    }
}