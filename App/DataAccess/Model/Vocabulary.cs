﻿using System.ComponentModel.DataAnnotations;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Vocabulary : IEntity
    {
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Název slovíčka je vyžadován.")]
        public virtual string Word { get; set; }

        [Required(ErrorMessage = "Popis slovíčka je vyžadován.")]
        public virtual string Description { get; set; }
    }
}