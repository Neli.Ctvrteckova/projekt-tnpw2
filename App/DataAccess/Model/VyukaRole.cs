﻿using DataAccess.Interface;

namespace DataAccess.Model
{
    public class VyukaRole : IEntity
    {
    public virtual int Id { get; set; }

    public virtual string Identificator { get; set; }

    public virtual string RoleDescription { get; set; }
    }
}