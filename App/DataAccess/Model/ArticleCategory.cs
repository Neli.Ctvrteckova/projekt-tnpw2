﻿using DataAccess.Interface;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Model
{
    public class ArticleCategory : IEntity
    {
        public virtual int Id { get; set; }
        [Required(ErrorMessage = "Název kategorie je vyžadován.")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Popis kategorie je vyžadován.")]
        public virtual string Description { get; set; }
    }
}