﻿using System;
using System.Web.Security;
using DataAccess.Interface;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Model
{
    public class VyukaUser : MembershipUser, IEntity
    {
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Vaše jméno je vyžádáno.")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Vaše příjmení je vyžadováno.")]
        public virtual string Surname { get; set; }

        [Required(ErrorMessage = "Vaši přezdívku musíte vyplnit.")]
        public virtual string Login { get; set; }

        [Required(ErrorMessage = "Vaše heslo musí být vyplněno.")]
        [StringLength(30, ErrorMessage = "Heslo musí obsahovat minimálně 5 znaků.", MinimumLength = 5)]
        public virtual string Password { get; set; }

        public virtual string Image { get; set; }

        public virtual DateTime? Birth { get; set; }

        public virtual DateTime RegistrationDay { get; set; }
        
        public virtual DateTime LastActivityDay { get; set; }

        public virtual VyukaRole Role { get; set; }

    }
}