﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Article : IEntity
    {        
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Název článku je vyžadován")]
        public virtual string Name { get; set; }

        [Required(ErrorMessage = "Autor článku je vyžadován")]
        public virtual string Author { get; set; }

        [AllowHtml]
        public virtual string Description { get; set; }

        public virtual int TestId { get; set; }

        public virtual ArticleCategory Category { get; set; }
    }
}