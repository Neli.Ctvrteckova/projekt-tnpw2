﻿using System;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class TestFinished : IEntity
    {
        public virtual int Id { get ; set; }

        public virtual int TestId { get; set; }

        public virtual int UserId { get; set; }

        public virtual int Score { get; set; }

        public virtual int MaxScore { get; set; }

        public virtual DateTime CompletingTest { get; set; }

    }
}