﻿using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class TestAnswer : IEntity
    {
        public virtual int Id { get; set; }

        public virtual TestQuestions Question { get; set; }

        public virtual string Answer { get; set; }

        public virtual bool IsCorrect { get; set; }

        [NotMapped]
        public virtual bool WasCorrect { get; set; }
    }
}