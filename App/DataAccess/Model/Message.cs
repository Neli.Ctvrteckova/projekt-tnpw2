﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Message : IEntity
    { 
        public virtual int Id { get; set; }

        [Required(ErrorMessage = "Váš e-mail je vyžadován.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Formát e-mailu je špatný.")]
        public virtual string Sender { get; set; }

        public virtual string Subject { get; set; }

        [Required(ErrorMessage = "Text nemůže být prázdný.")]
        public virtual string Text { get; set; }

        public virtual string UserName { get; set; }
    }
}