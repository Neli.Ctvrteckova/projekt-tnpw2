﻿using System.Collections.Generic;
using DataAccess.Interface;

namespace DataAccess.Model
{
    public class Test : IEntity
    {
        public virtual int Id { get; set; }

        public virtual bool Published { get; set; }

        public virtual IList<TestQuestions> Questions { get; set; }
    }
}