﻿using System.Collections.Generic;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class TestFinishedDao : DaoBase<TestFinished>
    {
        public IList<TestFinished> GetTestListByUserIdAndTestId(int userId, int testId)
        {
            return session.CreateCriteria<TestFinished>().
                Add(Restrictions.And(Restrictions.Eq("UserId", userId), Restrictions.Eq("TestId", testId)))
                .List<TestFinished>();
        }

        public IList<TestFinished> GetTestListByUserAndPage(int userId, int count, int page, out int totalWords)
        {
            totalWords = session.CreateCriteria<TestFinished>().SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return session.CreateCriteria<TestFinished>().
                Add(Restrictions.Eq("UserId", userId))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<TestFinished>();
        }

        public IList<TestFinished> GetTestFinshedByTestId(int testId)
        {
            return session.CreateCriteria<TestFinished>().
                Add(Restrictions.Eq("TestId", testId))
                .List<TestFinished>();
        }
    }
}
