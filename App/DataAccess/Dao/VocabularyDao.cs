﻿using System.Collections.Generic;
using DataAccess.Model;
using NHibernate.Criterion;

namespace DataAccess.Dao
{
    public class VocabularyDao : DaoBase<Vocabulary>
    {
        public IList<Vocabulary> GetVocabularyPaged(int count, int page, out int totalWords)
        {
            totalWords = session.CreateCriteria<Vocabulary>().SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return session.CreateCriteria<Vocabulary>()
                .AddOrder(Order.Asc("Word"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<Vocabulary>();
        }

        public IList<Vocabulary> SearchWord(string phrase)
        {
            return session.CreateCriteria<Vocabulary>()
                .Add(Restrictions.Like("Word", string.Format("%{0}%", phrase)))
                .List<Vocabulary>();
        }

        public Vocabulary GetByWord(string word)
        {
            return session.CreateCriteria<Vocabulary>()
                .Add(Restrictions.Eq("Word", word))
                .UniqueResult<Vocabulary>();
        }
    }
}