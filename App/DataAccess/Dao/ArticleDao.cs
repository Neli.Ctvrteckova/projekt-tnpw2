﻿using DataAccess.Model;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace DataAccess.Dao
{
    public class ArticleDao : DaoBase<Article>
    {    
        public IList<Article> SearchArticle(string phrase)
        {
            return session.CreateCriteria<Article>()
                .Add(Restrictions.Like("Name", string.Format("%{0}%", phrase)))
                .List<Article>();
        }

        public IList<Article> GetArticlesInCategoryId(int id)
        {
            return session.CreateCriteria<Article>()
                .CreateAlias("Category", "cat")
                .Add(Restrictions.Eq("cat.Id", id))
                .List<Article>();
        }

        public Article GetArticleByTestId(int testId)
        {
            return session.CreateCriteria<Article>()
                .Add(Restrictions.Eq("TestId", testId))
                .UniqueResult<Article>();
        }
    }
}