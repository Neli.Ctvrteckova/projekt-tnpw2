﻿using DataAccess.Model;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace DataAccess.Dao
{
    public class VyukaUserDao : DaoBase<VyukaUser>
    {
        public VyukaUser GetByLoginAndPassword(string login, string password)
        {
            return session.CreateCriteria<VyukaUser>()
                .Add(Restrictions.Eq("Login", login))
                .Add(Restrictions.Eq("Password", password))
                .UniqueResult<VyukaUser>();
        }

        public VyukaUser GetByLogin(string login)
        {
            return session.CreateCriteria<VyukaUser>()
                .Add(Restrictions.Eq("Login", login))
                .UniqueResult<VyukaUser>();
        }

        public IList<VyukaUser> GetUsersInCategoryId(int id)
        {
            return session.CreateCriteria<VyukaUser>()
                .CreateAlias("Role", "rol")
                .Add(Restrictions.Eq("rol.Id", id))
                .List<VyukaUser>();
        }

        public IList<VyukaUser> GetUsersPaged(int count, int page, out int totalUsers)
        {
            totalUsers = session.CreateCriteria<VyukaUser>().SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return session.CreateCriteria<VyukaUser>()
                .AddOrder(Order.Asc("Name"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<VyukaUser>();
        }

        public IList<VyukaUser> GetUsersByCategoryPaged(int id, int count, int page, out int totalUsers)
        {
            totalUsers = session.CreateCriteria<VyukaUser>()
                .CreateAlias("Role", "rol")
                .Add(Restrictions.Eq("rol.Id", id))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();

            return session.CreateCriteria<VyukaUser>()
                .CreateAlias("Role", "rol")
                .Add(Restrictions.Eq("rol.Id", id))
                .AddOrder(Order.Asc("Name"))
                .SetFirstResult((page - 1) * count)
                .SetMaxResults(count)
                .List<VyukaUser>();
        }

        public IList<VyukaUser> SearchUser(string phrase)
        {
            return session.CreateCriteria<VyukaUser>()
                .Add(Restrictions.Like("Login", string.Format("%{0}%", phrase)))
                .List<VyukaUser>();
        }

        public IList<VyukaUser> GetAdmins()
        {
            return session.CreateCriteria<VyukaUser>()
                .CreateAlias("Role", "rol")
                .Add(Restrictions.Eq("rol.Id", 1))
                .List<VyukaUser>();
        }

        public IList<VyukaUser> GetUsers()
        {
            return session.CreateCriteria<VyukaUser>()
                .CreateAlias("Role", "rol")
                .Add(Restrictions.Eq("rol.Id", 2))
                .List<VyukaUser>();
        }
    }
}